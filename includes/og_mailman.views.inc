<?php
/**
 * @file
 * Views integration for og_mailman
 */

/**
 * Implementation of hook_views_data().
 */
function og_mailman_views_data() {
  $data['og_mailman']['table']['group'] = t('OG Mailman');

  $data['og_mailman']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
      ),
  );

  $data['og_mailman']['email'] = array(
    'title' => t('Group Email'),
    'help' => t('The email address related to a group node'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator_string',
      'label' => t('Has Mailing List'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  return $data;
}
